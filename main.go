package main

import (
	"fmt"
	"log"
	"net/http"

	// "github.com/russross/blackfriday"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter().StrictSlash(false)
	r.HandleFunc("/", HomeHandler)

	// Posts collection
	posts := r.Path("/posts").Subrouter()
	posts.Methods("GET").HandlerFunc(PostsIndexHandler)
	posts.Methods("POST").HandlerFunc(PostsCreateHandler)

	// Posts singular
	post := r.PathPrefix("/posts/{id}").Subrouter()
	post.Methods("GET").Path("/edit").HandlerFunc(PostEditHandler)
	post.Methods("GET").HandlerFunc(PostShowHandler)
	post.Methods("PUT", "POST").HandlerFunc(PostUpdateHandler)
	post.Methods("DELETE").HandlerFunc(PostDeleteHandler)

	fmt.Println("Starting a server on :3000")
	// http.Handle("/tmp/",
	// http.FileServer(http.Dir("/tmp")))
	// http.ListenAndServe(":3000", nil)
	log.Fatal(http.ListenAndServe(":3000", http.FileServer(http.Dir("/Users/golan"))))

}

// HomeHandler is bullshit
func HomeHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "Home")
	log.Println(mux.Vars(r))
}

// PostsIndexHandler is
func PostsIndexHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "posts index")
	log.Println(mux.Vars(r))
}

// PostsCreateHandler is
func PostsCreateHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "posts index")
	log.Println(mux.Vars(r))
}

// PostShowHandler is
func PostShowHandler(rw http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	fmt.Fprintln(rw, "Showing post", id)
	log.Println("Showing post:", mux.Vars(r))
}

// PostUpdateHandler is
func PostUpdateHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "post update")
	log.Println(mux.Vars(r))

}

// PostDeleteHandler is
func PostDeleteHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "post delete")
	log.Println(mux.Vars(r))
}

// PostEditHandler is
func PostEditHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "Post edit")
	log.Println(mux.Vars(r))
}
